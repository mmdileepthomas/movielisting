// movies
export const FETCH_MOVIES = "fetch_movies"
export const WATCHED_MOVIE = "watched_movie"
export const SEARCH_STRING_STATUS = "search_string_status"
export const REMOVE_WATCHED_MOVIE = "remove_watched_movie"