export {
    fetchMovieList,
    watchedMovie,
    onChangeSearch,
    onRemoveMovie
} from  './movies'