import React from 'react'
import './logo.css'

const logo = props => {
    return (
        <div className="logo-container">
            <p>{props.logoText}</p>
        </div>
    )
}

export default logo